# Ansible role for PostgreSQL installation

## Introduction

[PostgreSQL](http://www.postgresql.org/) is an SQL database.

This role installs the server and initializes a default cluster.

Currently this role does not manage the configuration file.
The firewall configuration is left to your care.

## Variables

- **listen_addresses**: list of IP/DNS names to listen on (defaults to
                        localhost)
- **service_profile**: service "sizing" depending on expected load,
                       value in low/medium/high (defaults to low)
- **backup_databases**: list of databases names to backup daily

